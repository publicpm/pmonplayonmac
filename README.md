# README #

PlayOnMac or PlayOnLinux must already be installed!

To run
```
#!bash

./instantiate.sh Ubuntu 7.5
chmod 755 PatternMaker-7.5.sh 
./PatternMaker-7.5.sh 
```

PatternMaker-7.5.sh is the installation script.  It can be run directly from Bash on Ubuntu or through PlayOnLinux.  On the Mac it has to be run through PlayOnMac via Tools->Run a local script

*template.sh* contains the installation instructions for PatternMaker and MacroGenerator.  It can be modified to install additional programs.

Collections can be added by creating a new file under *patterns*.  Use an existing file as a template.