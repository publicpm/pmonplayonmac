#!/usr/bin/env /home/dad/Freelance/bash/pmonplayonmac/bash-3.2.57/bash

# There are a few important files
# template.sh is the "framework" of the installer
# main.cfg provides the URL of the main installer and the macro tool
# addons/ contains one file for each addon, bearing the NAME and URL fields.

function hashurl() {
    # TODO: Figure out sane caching
    wget -P ./downloads -c "${1}"
    md5sum "./downloads/$(basename "$1")" | cut -f1 -d' '
}

function onepattern() {
    local NAME
    local URL
    source "$1"
    NM=$( echo $NAME | sed 's/\//%2f/g' )
    echo "$URL"  > "$PATTERN_URLS/$NM"
    echo "$(hashurl "$URL" )" > "$PATTERN_HASHES/$NM"
}

REVISED="$(date +'%Y-%m-%d %H-%M')"
WINEVER="$(wine --version)"
WINEVER="${WINEVER#wine-}"
DISTRO="$1"
VERSION="$2"
INSTURL=""
MACROURL=""

CPATH="$(pwd)"
PATTERN_URLS="${CPATH}/pmpol/PATTERN_URLS"
PATTERN_HASHES="${CPATH}/pmpol/PATTERN_HASHES"
mkdir -p $PATTERN_URLS
mkdir -p $PATTERN_HASHES




# Usage: instantiate.sh <tested distro> <version>
# Example: instantiate.sh Ubuntu 7.5

source main.cfg

if [[ -n $INSTURL ]]; then
    INSTMD5="$(hashurl "$INSTURL")"
fi

if [[ -n $MACROURL ]]; then
    MACROMD5="$(hashurl "$MACROURL")"
fi

for i in patterns/*; do
    onepattern "$i"
done


sed \
    -e "s;#{REVISED};${REVISED};g" \
    -e "s;#{WINEVER};${WINEVER};g" \
    -e "s;#{DISTRO};${DISTRO};g" \
    -e "s;#{TYPE};${TYPE};g" \
    -e "s;#{INSTURL};${INSTURL};g" \
    -e "s;#{INSTMD5};${INSTMD5};g" \
    -e "s;#{MACROURL};${MACROURL};g" \
    -e "s;#{MACROMD5};${MACROMD5};g" \
    -e "s;#{CPATH};${CPATH};g" \
    template.sh > "PatternMaker-${VERSION}.sh"



