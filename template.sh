#!/usr/bin/env playonlinux-bash
# Date : (2015-05-17 23-19)
# Last revision : (#{REVISED})
# Wine version used : #{WINEVER}
# Distribution used to test : #{DISTRO}
# Author : Alex Elsayed <eternaleye@gmail.com>

# Must be run under PlayOn*
[[ -z "$PLAYONLINUX" ]] && exit 0

# Load the PlayOnLinux scripting library
source "$PLAYONLINUX/lib/sources"

WINEVERSION="1.6.2"
TITLE="PatternMaker"
PREFIX="PatternMaker"
# Note: This is assumed to end in /INSTALLER_NAME
INSTALLER_URL="#{INSTURL}"
INSTALLER_DIGEST="#{INSTMD5}"
MACROTOOL_URL="#{MACROURL}"
MACROTOOL_DIGEST="#{MACROMD5}"
DELETE_DOWNLOADS=""
CPATH="#{CPATH}"
# Create the setup window
POL_SetupWindow_Init

# Required
POL_Debug_Init

# Set up the initial page
POL_SetupWindow_presentation "$TITLE" "PatternMaker Software" "http://www.patternmakerusa.com" "Alex Elsayed" "$PREFIX"

declare -a PATTERNS_CHOSEN
PATTERN_URLS="${CPATH}/pmpol/PATTERN_URLS"
PATTERN_DIGESTS="${CPATH}/pmpol/PATTERN_HASHES"

    function prompt_patterns() {
	PU=$(ls -1 $PATTERN_URLS| paste -sd "~" | sed "s/%2f/\//g" )
        local IFS="~"
        POL_SetupWindow_checkbox_list "$(eval_gettext "What patterns would you like to install?")" "${TITLE}"  "$PU" "~"
        PATTERNS_CHOSEN=( $APP_ANSWER )
    }

    # Ask which patterns they want
    prompt_patterns


if [[ -n $MACROTOOL_URL ]]; then
    POL_SetupWindow_question "$(eval_gettext "Would you like to install the PatternMaker MacroGenerator?")" "$TITLE"
fi
if [[ ${APP_ANSWER} == FALSE ]]; then
    MACROTOOL_URL=""
fi

# Create a temporary directory we can use
POL_System_TmpCreate "${PREFIX}"
# ...and enter it
cd "${POL_System_TmpDir}"

# Install into the PatternMaker Wine environment
POL_Wine_SelectPrefix "${PREFIX}"

# Create the environment if it does not yet exist
POL_Wine_PrefixCreate

POL_SetupWindow_question "$(eval_gettext "Several installers will be downloaded, and may be beneficial to keep afterwards. Would you like to keep them?")" "$TITLE"
if [[ ${APP_ANSWER} == FALSE ]]; then
    DELETE_DOWNLOADS="1"
    POL_SetupWindow_message "$(eval_gettext "Understood. All downloaded files will be deleted when finished.")"
fi

POL_SetupWindow_message "$(eval_gettext "Downloading installers...")"
# Download the installer, verifying it against a digest
POL_Download_Resource "${INSTALLER_URL}" "${INSTALLER_DIGEST}" "${TITLE}"
if [[ -n $MACROTOOL_URL ]]; then
    POL_Download_Resource "${MACROTOOL_URL}" "${MACROTOOL_DIGEST}" "${TITLE}"
fi
for pat in "${PATTERNS_CHOSEN[@]}"; do
    p=$(echo $pat | tr " " "\ ")
    POL_Download_Resource $(cat "${PATTERN_URLS}/${p}") $(cat "${PATTERN_DIGESTS}/${p}") "${TITLE}"
done

# Apply a registry tweak to open PDF files in the native viewer
POL_Call POL_Function_SetNativeExtension "pdf"

# Run the installer
POL_Wine "${POL_USER_ROOT}/ressources/${TITLE}/$(basename "${INSTALLER_URL}")"
# And the macro installer
if [[ -n $MACROTOOL_URL ]]; then
    POL_Wine "${POL_USER_ROOT}/ressources/${TITLE}/$(basename "${MACROTOOL_URL}")"
fi
# And the pattern installers
for pat in "${PATTERNS_CHOSEN[@]}"; do
    POL_Wine "${POL_USER_ROOT}/ressources/${TITLE}/$(basename $(cat "${PATTERN_URLS}/${pat}"))"
done

# Create a shortcut
POL_Shortcut "PatVer7_5.exe" "${TITLE}"

if [[ -n $MACROTOOL_URL ]]; then
     POL_Shortcut "MacroGen4_5.exe" "${TITLE} MacroGenerator"
fi

POL_SetupWindow_Close

# Clean up after ourselves
POL_System_TmpDelete
[[ -n $DELETE_DOWNLOADS ]] && rm -r "${POL_USER_ROOT}/ressources/${TITLE}"

# The script will be signed; prevent bash from trying
# to run the signature
exit 0;

